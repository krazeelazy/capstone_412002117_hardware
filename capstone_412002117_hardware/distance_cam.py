#!/usr/bin/env python3

# @author: Mellissa Marie (412002117)

""" This script is used for the hardware component of my capstone project.
The script monitors a parking spot using an HC-SR04 Ultrasonic Sensor.
When an object is closer than threshold, an image is captured from a cellphone,
that is acting as an IP webcam.
When the object leaves, the arrival time, departure time and image name are stored
in a json file, so the data can be exported and analayzed. """

#may need to edit threshold (line 26) and webcam IP (line 29)

import numpy as np
import RPi.GPIO as GPIO
import time
import datetime
import urllib.request
import cv2
import json
import os.path
from os import path

#-------------------CONSTANTS-----------------------------
#: threshold distance in cm
THRESHOLD = 120

#: url used to capture the image from the Andoid "IP webcam"
url="http://192.168.137.41:8080/photo.jpg"

#data directory location (contains images and json file)
data_dir="/home/pi/Desktop/data/"

#GPIO Pins
#: the trigger pin for the ultrasonic sensor
GPIO_TRIGGER = 25 
#: the echo pin 
GPIO_ECHO = 24 
#: red led to signify that the spot is occupied (object is closer than threshold)
RED_LED = 20 
#: green led to signify that the spot is empty
GREEN_LED = 21 


#-------------------FUNCTIONS-----------------------------

def setup():
    """Sets up the GPIO pins
    """    
    GPIO.cleanup()
    
    #GPIO Mode (BOARD / BCM)
    GPIO.setmode(GPIO.BCM)

    GPIO.setwarnings(False)

    #set GPIO direction (IN / OUT)
    GPIO.setup(GPIO_ECHO, GPIO.IN)
    GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
    GPIO.setup(GREEN_LED, GPIO.OUT)
    GPIO.setup(RED_LED, GPIO.OUT)

    #set output pins to low by default
    GPIO.output(GPIO_TRIGGER, False)
    GPIO.output(GREEN_LED, False)
    GPIO.output(RED_LED, False)

    return


def distance():
    """Measures the distance

    :return: how far away the vehicle is from the sensor in cm.  
    :rtype: float
    """    
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)
    
    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    start_time = time.time()
    stop_time = time.time()

    # save start_time
    while GPIO.input(GPIO_ECHO) == 0:
        start_time = time.time()

    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        stop_time = time.time()

    # time difference (in seconds) between start and arrival/stop
    time_elapsed = stop_time - start_time
    
    # multiply with the sonic speed (34300 cm/s) and
    # divide by 2, because journey is there and back
    distance = (time_elapsed * 34300) / 2

    return distance


#function to 
def check_today_folder(today):
    """Checks if there's a directory with today's date inside the data directory. 
    Create it if it doesn't exist

    :param today: today's date  
    :type today: str
    """    

    if not os.path.exists(f"{data_dir}{today}"): #if there is no directory for today
        os.makedirs(f"{data_dir}{today}/images/") #create the directory and the images sub-directory
    return

def capture(arrival_time):
    """Capture, name and save the image from the "IP webcam"

    :param arrival_time: the arrival time of the vehicle  
    :type arrival_time: str  
    :return:  the name of the image created  
    :rtype: str
    """    

    img_path=urllib.request.urlopen(url) #capture the image
    img_np=np.array(bytearray(img_path.read()),dtype=np.uint8) #convert it to an array
    img=cv2.imdecode(img_np,-1) #decode the array to a format that opencv can handle
    today = datetime.date.today().strftime("%Y-%m-%d") #get today's date in the format: year-month-day
    check_today_folder(today) #check if a directory exists for today, else create it
    img_parent = f"{data_dir}{today}/images/" #the location of the images directory
    img_name = f"{arrival_time}.jpg" #the name of the image format: year-month-day-hour:minute:second.jpg
    cv2.imwrite(f"{img_parent}{img_name}",img) #save the image
    return img_name #return the name of the image to the main program


def write_json(data, filename): 
    """Writes data to a json file

    :param data: the data that has to be added to the json file  
    :type data: list  
    :param filename: the absolute path to the json file  
    :type filename: str
    """

    with open(filename,'w+') as f: #open the file in write mode and create it if it doesn't already exist
        json.dump(data, f, indent=4) #store the json data in the file
    return

def save_to_file(arrival_time, departure_time, img_name):
    """Saves the arrival_time, departure_time, and img_name to today's json file

    :param arrival_time: the vehicle's arrival time  
    :type arrival_time: str  
    :param departure_time: the vehicle's departure time  
    :type departure_time: str  
    :param img_name: the name of the vehicle's image  
    :type img_name: str
    """

    today = datetime.date.today().strftime("%Y-%m-%d") #today's date (year-month-day)
    today_file = f"{data_dir}{today}/{today}.json" #the name of today's json file

    #check if a json file already exists for today
    if path.exists(today_file): #if it exists open it and append the new data to the data array
        with open(today_file) as json_file:
            data = json.load(json_file)

            # python object to be appended 
            new_data = {
                        "arrival_time":arrival_time,
                        "departure_time":departure_time,
                        "image":img_name
                        }

            data.append(new_data) # append the new data to data array

        write_json(data,today_file) #save the file  
            
    else: #if today's json file doesn't exist yet create a new file with the following content
        data = [
                {
                    "arrival_time":arrival_time,
                    "departure_time":departure_time,
                    "image":img_name
                }
            ]
        

        write_json(data,today_file) #save the file 
    return


def main():
    """The main program. Calls all other functions. Loops until the script stops running.
    """

    #setup the distance sensor and leds
    setup()
    
    try:
        while True: #loop infinitely
            dist = distance() #get the current distance

            if dist <= THRESHOLD: #if the object is closer than threshold
                GPIO.output(GREEN_LED, False) #turn off the green light
                GPIO.output(RED_LED, True) #turn on the red light
                arrival_time =  datetime.datetime.today().strftime("%Y-%m-%d-%H:%M:%S") #arrival time format: year-month-day-hour:minute:second
                img_name = capture(arrival_time) #capture, name and save the image
                
                while dist <= THRESHOLD: #while the object is closer than threshold
                    dist = distance() #update the distance measurement

                    if dist > THRESHOLD: #if they leave the spot           
                        GPIO.output(GREEN_LED, True) #turn on the green light
                        GPIO.output(RED_LED, False) #turn off the red light
                        departure_time = datetime.datetime.today().strftime("%Y-%m-%d-%H:%M:%S") #departure time format: year-month-day-hour:minute:second
                        save_to_file(arrival_time, departure_time, img_name) #save the arrival time, departure time and image name to today's json file
                    else: #if they're still in the spot
                        time.sleep(1) #pause
            else: #if there's no object closer than threshold
                GPIO.output(GREEN_LED, True) #turn on the green light
                GPIO.output(RED_LED, False) #turn off the red light
                time.sleep(1) #pause

    # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
    return


#-----------------------RUN MAIN PROGRAM-------------------
if __name__ == '__main__':
    main()
