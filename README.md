capstone\_412002117\_hardware
=============================

capstone\_412002117\_hardware contains the Python script
(distance\_cam.py) for the hardware component of the capstone project
by Mellissa Marie (412002117).

Background
----------

### Purpose

This is the hardware component of my capstone project (Parking Spot
Analysis - PSA).

According to the “Green Economy Scoping Study Barbados” published by
the United Nations Environment Programme, congestion within Bridgetown
is caused by on-street parking due to limited off-street parking
facilities. They suggest that a system of on-street parking charges
and regularization of length-of-stay in authorized parking areas
should be considered as measures to address the problem. This project
focuses on the length-of-stay and authorized parking areas part of
this.

At a meeting with personnel at the Barbados Ministry of Transport and
Works to try to narrow down the scope of this project from the broad
topic of general parking to something that they have a need for and
would be useful to them, several things were brought up. First, there
isn’t a system to centralize the control or regulation (of prices) of
the current off-street parking structures and facilities. Second, they
need research to be done to analyse the usage of delivery spots
(parking spots designated for use by delivery trucks). More
specifically, what vehicles are parking in the spots (delivery trucks
or private vehicles) and how long the vehicles are parking in the
spots.

Currently, if they want to conduct this type of research, it has to be
done manually. Where a person either goes to the area and manually
monitors the metrics they are trying to measure or a person sits and
watches recorded video to get the data.

The goal of this project is to automate the data capture and make it
easier to get useful information from the data captured. This part of
the project, hardware component, deals with the automated monitoring
of a parking spot.

Overview
--------

My solution involves using IoT to detect the arrival of a vehicle in a
parking spot, identify some characteristics about the vehicle (such as
type of vehicle), detect the departure of the vehicle from the spot
and thus determine the length-of-stay. The solution has several parts:
1) a sensor to detect the presence of the vehicle, 2) a camera to
capture an image of the vehicle which will be analysed to determine
the characteristics of the vehicle, 3) a control module to connect the
sensors to the camera so the camera can be triggered by the sensor and
4) the centralized control centre and data store to store the data and
analyse it.

The hardware component uses an HC-SR04 Ultrasonic Sensor to detect the
presence of the vehicle. It uses a recycled Android phone with and IP
webcam app as the camera. It uses a Raspberry Pi 3B as the control
module. The Pi has a wired connection to the sensor, but a wireless
one (over WIFI) to the "IP webcam". The sensor was placed close to the
ground to ensure it detected the vehicle, while the camera was
placed higher and angled down to ensure a good angle was used for the
pictures.

The system works in the following way:

1.  Vehicle detected:    
    a.  Store the current time in a variable    
    b.  Capture the webcam image    
    c.  Use the arrival time variable to name the image    
    d.  Store the image on the Pi    

2.  Vehicle leaves:    
    a.  Store departure time in a variable    
    b.  Store the arrival and departure times along with the image name in a Json file

3.  At the end of the day export the file and images so they can be analysed

The data is collected from the node manually and uploaded to the data
store so it can be analysed. The centralised store (my laptop) had
scripts (the software component) that analyse the data to determine
the length-of-stay and analyse the images to determine the
characteristics of the vehicle. This analysed information was uploaded
to a database that can be accessed via a web application.

Requirements
------------

### Hardware

*   Raspberry Pi 3B (with SD card) (x1)
*   Power Supply for Pi (20000 mAh powerbank/powerstation was used) (x1)
*   Breadboard (x1)
*   HC-SR04 Ultrasonic Sensor (x1)
*   Female/Male 'Extension' Jumper wires (x6)
*   Short piece of solid core wire (x1)
*   Resistors ( 220 Ω x2; 1 k Ω x1; 2 k Ω x1)
*   LEDs (Red x1; Green x1) (for testing and visualization)
*   Phone with IP webcam app installed

### Software

#### Python version

Python 3.7

#### Python packages used

*   numpy
*   RPi.GPIO (built into Raspbian OS)
*   time
*   datetime
*   urllib
*   cv2
*   json
*   os.path
*   os

(sphinx was used for documentation)

Usage
-----

**Important: The phone (webcam) and the PI must be on the same wireless network**

**Warning: Some of the file names for the images used in the documentation are long, so you may get an error if you try to download the project on a Windows system.**

1.  Install an IP webcam app on the phone ([this](https://play.google.com/store/apps/details?id=com.pas.webcam&hl=en) one was used on an Android phone).

2.  Start the webcam server and note the camera's IP address

3. Update the script with appropriate values for some constants:  
    a.  THRESHOLD (minimum distance in cm)   
    b.  url (the url that is used to capture the image; e.g.<http://webcam.ip.address:8080/photo.jpg)    
    c.  data\_dir (where you want the data to be stored)

4.  From the PI's command line:
        
        cd dir/containing/script
        
        python3 distance_cam.py # run the script

Useful Links
------------

1.  [Installing opencv on Raspberry Pi](https://www.pyimagesearch.com/2019/09/16/install-opencv-4-on-raspberry-pi-4-and-raspbian-buster/)

2. Software component:

    a. Code on [Bitbucket](https://bitbucket.org/krazeelazy/capstone_412002117_software/src/master/)

    b. Docs on [Read the Docs](https://capstone-412002117-software.readthedocs.io/en/latest/index.html)

3. Hardware component:

    a. Code on [Bitbucket](https://bitbucket.org/krazeelazy/capstone_412002117_hardware/src/master/)

    b. Docs on [Read the Docs](https://capstone-412002117-hardware.readthedocs.io/en/latest/index.html)

