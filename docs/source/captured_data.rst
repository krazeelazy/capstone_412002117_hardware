.. _Captured Data:

Captured Data
=============

Video of capturing data
~~~~~~~~~~~~~~~~~~~~~~~

The following video shows the hardware test.

.. important:: Video is best viewed in fullscreen


.. raw:: html

    <video width=100% height=100% controls>
        <source src="_static/capstone_412002117_hardware_test.mp4" type="video/mp4">
        Your browser does not support the video tag.
    </video> 


.. note:: Video file can be found in the video folder: capstone_412002117_hardware/video/capstone_412002117_hardware_test.mp4


Json Data
~~~~~~~~~

.. code-block:: json

    [
        {
            "arrival_time": "2020-03-18-13:14:38",
            "departure_time": "2020-03-18-13:16:32",
            "image": "2020-03-18-13:14:38.jpg"
        },
        {
            "arrival_time": "2020-03-18-13:18:41",
            "departure_time": "2020-03-18-13:24:52",
            "image": "2020-03-18-13:18:41.jpg"
        },
        {
            "arrival_time": "2020-03-18-13:26:11",
            "departure_time": "2020-03-18-13:37:01",
            "image": "2020-03-18-13:26:11.jpg"
        }
    ]


Images
~~~~~~

The images that were captured are shown below. The images weren't altered, no matter the selected orientation, the captured image is always landscape.

.. figure:: ../../images/captured/2020-03-18-13_26_11.jpg
    :alt: 2020-03-18-13_26_11.jpg

    2020-03-18-13_26_11.jpg

.. figure:: ../../images/captured/2020-03-18-13_18_41.jpg
    :alt: 2020-03-18-13_18_41.jpg

    2020-03-18-13_18_41.jpg

.. figure:: ../../images/captured/2020-03-18-13_14_38.jpg
    :alt: 2020-03-18-13_14_38.jpg

    2020-03-18-13_14_38.jpg