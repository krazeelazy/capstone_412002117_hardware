# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.append(os.path.abspath('../..'))


# -- Project information -----------------------------------------------------

project = 'capstone_412002117_hardware'
copyright = '2020, Mellissa Marie'
author = 'Mellissa Marie'

# The full version, including alpha/beta/rc tags
release = '1.0.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.imgmath',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'nature'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- Options for autodoc -----------------------------------------------------
autodoc_mock_imports = ['RPi']

autodoc_member_order = 'bysource'

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
add_module_names = False


html_logo = '../../images/logo/white_bg.png'

html_favicon = '../../images/logo/favicon.ico'



# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
    'figure_align': 'H',
}

# Grouping the document tree into LaTeX files. List of tuples# (source start file, target name, title, author, documentclass [howto/manual]).
latex_documents = [('index', 'capstone_412002117_hardware_documentation.tex', u'Capstone 412002117 Hardware Documentation', u'Mellissa Marie', 'manual'),]

latex_logo = '../../images/logo/white_bg.png'