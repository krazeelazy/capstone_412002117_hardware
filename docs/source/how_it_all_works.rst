How it all works
================

Hardware
--------

Wiring Diagram
~~~~~~~~~~~~~~

.. figure:: ../../images/device/capstone_wiring_bb_latest.png
    :alt: Wiring diagram

    Wiring diagram

.. figure:: ../../images/device/capstone_wiring_schem.png
    :alt: Schematic diagram

    Schematic diagram


Constructed Device
~~~~~~~~~~~~~~~~~~

.. figure:: ../../images/device/top.jpg
    :alt: Device top view

    Device top view

.. figure:: ../../images/device/top_close.jpg
    :alt: Device closer top view

    Device closer top view

.. figure:: ../../images/device/front.jpg
    :alt: Device front view

    Device front view


Theoretical Setup
~~~~~~~~~~~~~~~~~

.. figure:: ../../images/setup/theoretical/theoretical_hw_setup.png
    :alt: Theoretical Setup Diagram

    Theoretical Setup Diagram


Actual Setup
~~~~~~~~~~~~

.. figure:: ../../images/setup/actual/actual_hw_setup.png
    :alt: Actual Setup Diagram

    Actual Setup Diagram

.. figure:: ../../images/setup/actual/actual_hw_setup_full.jpg
    :alt: Actual Setup

    Actual Setup

.. figure:: ../../images/setup/actual/actual_device_laid_out.jpg
    :alt: Actual device laid out

    Actual device laid out

.. figure:: ../../images/setup/actual/actual_device_front.jpg
    :alt: Actual device front

    Actual device front

.. figure:: ../../images/setup/actual/actual_device_front_closer.jpg
    :alt: Actual device front closer

    Actual device front closer

.. figure:: ../../images/setup/actual/actual_device_front_closests.jpg
    :alt: Actual device front closest

    Actual device front closest

.. figure:: ../../images/setup/actual/actual_device_front_left.jpg
    :alt: Actual device front left

    Actual device front left

.. figure:: ../../images/setup/actual/actual_device_front_right.jpg
    :alt: Actual device front right

    Actual device front right

.. figure:: ../../images/setup/actual/actual_device_side.jpg
    :alt: Actual device side

    Actual device side

.. figure:: ../../images/setup/actual/actual_device_back.jpg
    :alt: Actual device back

    Actual device back

.. figure:: ../../images/setup/actual/actual_phone_on_fence.jpg
    :alt: Actual phone on fence

    Actual phone on fence

.. figure:: ../../images/setup/actual/actual_phone_on_fence_closer.jpg
    :alt: Actual phone on fence closer

    Actual phone on fence closer

.. figure:: ../../images/setup/actual/actual_phone_on_fence_closer_side.jpg
    :alt: Actual phone on fence closer side

    Actual phone on fence closer side

.. figure:: ../../images/setup/actual/actual_phone_on_fence_closer_back_1.jpg
    :alt: Actual phone on fence closer back 1

    Actual phone on fence closer back 1

.. figure:: ../../images/setup/actual/actual_phone_on_fence_closer_back_2.jpg
    :alt: Actual phone on fence closer back 2

    Actual phone on fence closer back 2

.. figure:: ../../images/setup/actual/actual_phone_arm_band_on_fence.jpg
    :alt: Actual phone arm band on fence

    Actual phone arm band on fence

.. figure:: ../../images/setup/actual/actual_phone_arm_band.jpg
    :alt: Actual phone arm band

    Actual phone arm band

Ultrasonic Sensor theory
~~~~~~~~~~~~~~~~~~~~~~~~
The image below shows the theory of the sensor.  Ultrasonic sensors generally have two (2) parts, an emitter/transmitter/speaker and a receiver/microphone. 
The sensor sends a brief chirp with its ultrasonic speaker (triggered by the trigger/trig pin) and measures the echo's return time (return relayed by 
the echo pin) to its ultrasonic microphone. Using this measurement and the speed of sound in air, an object's distance can be calculated in cm, inches, 
feet, etc (cm was used in this case).

.. figure:: ../../images/device/hc-sr04.png
    :alt: HC-SR04 Ultrasonic Sensor theory

    HC-SR04 Ultrasonic Sensor theory  



Software
--------

Constants
~~~~~~~~~

1. THRESHOLD: the threshold distance in cm that will trigger the Pi to capture the webcam's image.  

2. url: the url that has to be accessed to capture the image from the phone ("IP webcam")  

3. data_dir: the location of the data directory where the images and json files are stored.  

4. GPIO_TRIGGER: the Pi pin connected to the trigger pin of the ultrasonic sensor

5. GPIO_ECHO: the Pi pin connected to the echo pin of the ultrasonic sensor  

6. RED_LED: the Pi pin connected to the red LED (to signify that the spot is occupied (object is closer than threshold))  

7. GREEN_LED: the Pi pin connected to the green LED (to signify that the spot is empty)  


Setup
~~~~~

Uses the :func:`capstone_412002117_hardware.distance_cam.setup` function. Sets up the GPIO pins of the Pi. Sets GPIO mode to BCM. Sets the echo pin as an input and the trigger, red LED and green LED pins as outputs. 
Finally the three (3) output pins are set to low by default .  


Measuring the distance
~~~~~~~~~~~~~~~~~~~~~~

Within an infinite (keyboard  interruptible) loop, the distance is measured using the :func:`capstone_412002117_hardware.distance_cam.distance` function. If the distance is greater than threshold, then the green light is turned on 
if it wasn't on previously and the red one is turned off if it was on before, then the system pauses for 1 second and loops again.  

If the distance is less than or equal to threshold, the green light is turned off and red is turned on, the arrival time is stored in a variable, the phone's 
("IP webcam's") image is captured using the url constant and saved, and the name of the image is stored in a variable. The distance is continuously checked and if it becomes 
more than threshold (the vehicle leaves the parking spot), then the green light is turned on while the red is turned off, and the departure time is stored in a variable. 
Finally, the arrival and departure times along with the image name are stored in a json file. Then the program goes back to the loop that checks to see when the distance becomes 
less than threshold.  

The way the :func:`capstone_412002117_hardware.distance_cam.distance` function works is by:

1. Set the Trigger pin to High
2. Wait 0.01 ms and set the Trigger pin to low
3. Measure the amount of time it takes for a "ping" emitted by the transmitter to bounce off of the object and then be reflected to the  receiver part of the sensor
4. Multiply the elapsed time by the speed of sound (34300 cm/s) and divide this value by 2 (to account for the fact that the journey was to the object and back).  
   This value is the distance in cm. 
   Equation:

   .. math::

    Distance =   \frac{elapsed time * 34300}{2}

Storing the Data
~~~~~~~~~~~~~~~~

Image  
    The :func:`capstone_412002117_hardware.distance_cam.capture` function is used to store the image. The url constant is used in a url request to capture the image from the phone. Then the image is converted into a format 
    that OpenCV can use. The current date is stored in a variable names "today" which is passed to the :func:`capstone_412002117_hardware.distance_cam.check_today_folder` function which just checks if there's a directory named after 
    today's date in the data directory (data_dir constant) and creates it and an images sub directory if not. OpenCV is then used to create and store the image in the images sub directory. 
    The image's name ('arrival_time'.jpg) is passed back to the main function. 

Json data  
    The :func:`capstone_412002117_hardware.distance_cam.save_to_file` function is used to store the data in a json file. The name of the json file is created using this format: data_dir/{today}/{today}.json. If the file already exists then the data is appended 
    to the json file :
    
    .. code-block:: python

        new_data = {
                        "arrival_time":arrival_time,
                        "departure_time":departure_time,
                        "image":img_name
                    }
    
    else a new file is created with the data in the following format:
    
    .. code-block:: python

        data = [
                    {
                        "arrival_time":arrival_time,
                        "departure_time":departure_time,
                        "image":img_name
                    }
                ]


The captured data can be seen :ref:`here <Captured Data>`.